<div id="punwrap">
    <div id="punForum" class="pun">

        {if $mode == "new"}
            {include file="mw-new.tpl"}
        {/if}

        <div id="idx1" class="blocktable">
            <h2><span class="conr"><a href="{$path|escape:'html'}/new">Post New Topic</a></span>
                Milliways Topic List for {$folder|escape:'html'}</h2>

            <div class="box">
                <table cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col" class="tc1">Topic</th>
                        <th scope="col" class="tc2">Replies</th>
                        <th scope="col" class="tcr">Last post</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$articles item=art}
                        <tr>
                            <td class="tcl">
                                <div class="intd">
                                    <div class="icon">
                                        <div class="nosize"><!-- --></div>
                                    </div>
                                    <div class="tclcon">
                                        <a href="{$path|escape:'html'}/{$art.ref}">{if $art.subject == ""}
                                                <i>No Subject</i>
                                            {else}{$art.subject|escape:'html'}{/if}</a> by {$art.from|escape:'html'}
                                    </div>
                                </div>
                            </td>
                            <td class="tc2">{$art.sub}</td>
                            <td class="tcr">
                                {if $art.sub == 0}
                                    {$art.date|date_format}
                                {else}
                                    {$art.lastpost|date_format} by {$art.lastfrom|escape:'html'}
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
