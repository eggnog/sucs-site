{include file="../static/fragments/Front.txt"}

{if count($news) gt 0}
    <div id="news">
        <div class="box">
            <div class="boxhead"><h2>Latest Announcements</h2></div>
            <div class="boxcontent">
                {foreach name=news from=$news key=itemnum item=item}
                    <h3>{$item.title}</h3>
                    <div>
                        {articlesummary id=$item.id article=$item.body title=$item.title}
                    </div>
                {/foreach}
                <div class="clear"></div>
            </div>
            <div class="hollowfoot">
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
{/if}
