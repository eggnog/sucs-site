{include file="../static/fragments/Events.txt"}
<div class="box">
    <div class="boxhead">
        <h3>Got an idea?</h3>
    </div>
    <div class="boxcontent">
        <p>Got an idea for a event you'd like to see hosted?
            Email {mailto address="exec@sucs.org" encode='javascript'}</p>
    </div>
    <div class="hollowfoot">
        <div>
            <div></div>
        </div>
    </div>
</div>

{if $editable}
    <a href="?action=create">Add Event</a>
{/if}

<div class="box">
    <div class="boxhead"><h2>Upcoming Events</h2></div>
    <div class="boxcontent">
        {if $events|@count < 1}
            <p>No events :(</p>
            <p>Pester {mailto address="exec@sucs.org" encode='javascript'} to organise some!</p>
        {else}
            {foreach from=$events item=event}
                <h2>
                    <a href="{$baseurl}/Community/Events/{$event.category}_{$event.id}">{$event.name}</a>
                </h2>
                <div>
                    <b>{$event.whn_timestamp|date_format:"l, jS F Y"}{if $event.show_time},{$event.whn_timestamp|date_format:"%H:%M"}{/if}</b>
                </div>
                <div>at <b>{$event.location}</b></div>
                <div><p>{$event.description}</p></div>
            {/foreach}
        {/if}
    </div>
    <div class="hollowfoot">
        <div>
            <div></div>
        </div>
    </div>
</div>

<hr/>

<h2>Recent Events</h2>
<p>
    <small>You missed them. Too bad!</small>
</p>
{foreach from=$oldevents item=event}
    <h4><a href="{$baseurl}/Community/Events/{$event.category}_{$event.id}">{$event.name}</a> -
        <small>{$event.whn_timestamp|date_format:"%A, %e %B"}</small>
    </h4>
{/foreach}
