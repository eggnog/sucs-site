{* form logged-in members can fill in to give us feedback on a website page *}

<div class="cbb">
    {if $feedbacked == true}
        <h3>Thanks!</h3>
        <p>Your comments have been sent to the SUCS admin team</p>
    {else}
        <h3>Something you think we should know?</h3>
        <form name="feedback" action="{$baseurl}{$path}" method="post">
            <input type="hidden" name="action" value="feedback"/>

            <p>Spotted a mistake on this page? Have some useful info? Let us know below:</p>

            <div>
                <span class="textinput"><textarea id="pagefeedback" name="feedback" cols="10"
                                                  rows="6"></textarea></span>
            </div>
            <div>
                <span class="textinput"><input type="submit" value="Feed back!"/></span>
            </div>
        </form>
    {/if}
</div>
