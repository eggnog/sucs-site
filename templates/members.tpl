{if isset($members) }
    {if isset($results) }
        {if count($results) > 1 }
            <p>There are several matches for your query, please select one below - </p>
            <ul>
                {foreach name=results from=$results item=result}
                    <li><a href="{$url}/{$result.uid}">{$result.uid}</a></li>
                {/foreach}
            </ul>
        {elseif count($results) == 1 }
            {assign var=member value=$results[0]}
            <div class="box">
                <div class="boxhead"><h2>Membership Details for {$member.uid}</h2></div>
                <div class="boxcontent">
                    {if isset($member.picture) }
                        <img class="emblem" src="{$baseurl}{$member.picture}" alt="Picture of {$member.uid}"/>
                    {/if}
                    <dl class="member">
                        <dt>Username</dt>
                        <dd>{$member.uid|escape:'htmlall'}</dd>
                        <dt>Real Name</dt>
                        <dd>{$member.cn|escape:'htmlall'}</dd>
                        <dt>Account Type</dt>
                        <dd>{$member.acctype|escape:'htmlall'}</dd>
                        {if $member.website }
                            <dt>Website</dt>
                            <dd>
                                <a href="http://sucs.org/~{$member.uid|escape:'url'}/">http://sucs.org/~{$member.uid|escape:'url'}/</a></dd>
                        {/if}
                        {if isset($member.project) }
                            <dt>Project</dt>
                            <dd id="project">{$member.project|escape:'htmlall'}</dd>
                        {/if}
                        {if isset($member.plan) }
                            <dt>Plan</dt>
                            <dd id="plan">{$member.plan|escape:'htmlall'}</dd>
                        {/if}

                        <dt>Banana Score</dt>
                        {if $member.awardsbyyear|@count >= 1}
                            <dd>{bananaprint score=$member.bananascore}{$member.bananascore}
                                banana{if $member.bananascore<>1}s{/if}</dd>
                        {else}
                            <dd>{$member.uid} doesn't have any banana awards</dd>
                        {/if}
                    </dl>


                    <div class="clear"></div>
                </div>
                <div class="hollowfoot">
                    <div>
                        <div></div>
                    </div>
                </div>
            </div>
            {if $member.awardsbyyear|@count >= 1}
                <div class="box">
                    <div class="boxhead"><h2>Banana Awards for {$member.uid}</h2></div>
                    <div class="boxcontent">
                        <dl class="bananas">
                            {foreach name=awardsbyyear from=$member.awardsbyyear key=yearname item=year}
                                <dt>{$yearname}/{math equation="x + 1" x=$yearname}:
                                    <small>{$year.sum} banana{if $year.sum|abs != 1}s{/if}</small>
                                </dt>
                                <dd>{include file='banana-awardlist.tpl' awards=$year.awards singlemember=true}</dd>
                            {/foreach}
                        </dl>
                    </div>
                    <div class="hollowfoot">
                        <div>
                            <div></div>
                        </div>
                    </div>
                </div>
            {/if}
        {else}
            <div class="errorbar">
                <div>
                    <div>
                        <div>
                            Members: No Results (Invalid Search)
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {else}
        <p> Please search for a member in the sidebar.</p>
        <h3>Recent Awards</h3>
        {include file='banana-awardlist.tpl' awards=$stats.recent}
    {/if}
{else}
    <p>Please log in to view full member details.</p>
    <h3>Members with public websites:</h3>
    <div class="memberlist">
        {memberwebsitelist  members=$public_members}
    </div>
{/if}

