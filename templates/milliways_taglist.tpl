<table cellspacing="20">
    <tr>
        <td valign="top">
            <table>
                <tr>
                    <th colspan="2">Most Popular</th>
                </tr>
                {foreach from=$toplist item=row name=toplist}
                    <tr>
                        <td nowrap="nowrap" class="user">
                            <a href="{$mwpath}/tag/{$row.name|escape:'url'}">{$row.tag}</a>
                        </td>
                        <td class="time">{$row.count}</td>
                    </tr>
                {/foreach}
            </table>

        </td>
        <td valign="top">
            <table>
                <tr>
                    <th colspan="2">Latest</th>
                </tr>
                {assign var=lastday value=''}
                {foreach from=$latestlist item=row name=list}
                    {if $row.day != $lastday}
                        {assign var=lastday value=$row.day}
                        <tr>
                            <td nowrap="nowrap" class="date" colspan="3">{$row.dated|date_format:'%d %b %Y'}</td>
                        </tr>
                    {/if}
                    <tr>
                        <td nowrap="nowrap" class="user">
                            <a href="{$mwpath}/tag/{$row.name|escape:'url'}">{$row.tag}</a>
                        </td>
                        <td nowrap="nowrap" class="time">{$row.dated|date_format:'%H:%M:%S'}</td>
                    </tr>
                {/foreach}
            </table>
        </td>
    </tr>
</table>
