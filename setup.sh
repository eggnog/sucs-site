#!/bin/sh

# Setup script for SUCS website in users' public_html dirs
echo "SUCS website setup script"

ls $(pwd)/setup.sh &> /dev/null
if [ $? -ne 0 ]; then echo "Please run this script from its containing directory"; exit; fi

# generate correct .htaccess
echo -ne "\n\nGenerating .htaccess file..."
sed -e "s/\\\$USERNAME/$USER/" userhtaccess > htdocs/.htaccess
echo "Done!"

# generate settings file
echo -n "Generating settings file..."
sed -e "s:\\\$BASEDIR:$PWD:;s/\\\$USERNAME/$USER/" settingstemplate > htdocs/settings.php
echo "Done!"

# place a link in public_html
echo -n "Linking to public_html..."
ln -s $PWD/htdocs $HOME/public_html/sucssite
echo "Done!"

# and you're done!
echo -e "\nYou should now be able to view your local copy of the SUCS site at http://sucs.org/~$USER/sucssite/"
echo "Please contact a member of the admin team to gain access to the development database"