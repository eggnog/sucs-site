<?php

// map number of bananas (-3 to 3) to a CSS class name
function smarty_function_award_image_class($params, &$smarty)
{
    $number = $params['number'];

    $image_class = "";

    switch ($number) {
        case -3:
            $image_class = "green3";
            break;
        case -2:
            $image_class = "green2";
            break;
        case -1:
            $image_class = "green1";
            break;
        case  1:
            $image_class = "yellow1";
            break;
        case  2:
            $image_class = "yellow2";
            break;
        case  3:
            $image_class = "yellow3";
            break;
    }

    return $image_class;
}

?>
