<?php
function smarty_function_bananaprint($params, &$smarty)
{
    $output = "";
    $score = $params['score'];
    if ($score > 0) {
        while ($score >= 250) {
            $output .= " <img src=\"/images/bananas/banana-container.png\" width=\"92\" height=\"64\" alt=\"250\" title=\"Container of 250 Bananas\" />\n";
            $score -= 250;
        }
        while ($score >= 50) {
            $output .= " <img src=\"/images/bananas/banana-crate.png\" width=\"92\" height=\"64\" alt=\"50\" title=\"Crate of 50 Bananas\" />\n";
            $score -= 50;
        }
        while ($score >= 5) {
            $output .= " <img src=\"/images/bananas/banana-bunch.png\" alt=\"5\" width=\"64px\" height=\"64px\" title=\"Bunch of 5 Bananas\" />\n";
            $score -= 5;
        }
        while ($score > 0) {
            $output .= " <img src=\"/images/bananas/banana-one.png\" alt=\"1\" width=\"25px\" height=\"64px\" title=\"1 Banana\" />\n";
            $score -= 1;
        }
    } else if ($score == 0) {
        $output .= " <img src=\"/images/bananas/banana-zero.png\" alt=\"0\" width=\"25px\" height=\"64px\" title=\"0 Bananas\" />\n";
    } else {
        while ($score <= -250) {
            $output .= " <img src=\"/images/bananas/banana-g-container.png\" width=\"92\" height=\"64\" alt=\"250\" title=\"Container of 250 Green Bananas\" />\n";
            $score += 250;
        }

        while ($score <= -50) {
            $output .= " <img src=\"/images/bananas/banana-g-crate.png\" width=\"92\" height=\"64\" alt=\"-50\" title=\"Crate of 50 Green Bananas\" />\n";
            $score += 50;
        }
        while ($score <= -5) {
            $output .= " <img src=\"/images/bananas/banana-g-bunch.png\" width=\"64px\" height=\"64px\" alt=\"-5\" title=\"Bunch of 5 Green Bananas\" />\n";
            $score += 5;
        }
        while ($score < 0) {
            $output .= " <img src=\"/images/bananas/banana-g-one.png\" width=\"25px\" height=\"64px\" alt=\"-1\" title=\"1 Green Banana\" />\n";
            $score += 1;
        }
    }

    return $output;
}

?>
