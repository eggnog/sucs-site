<p><strong>Note:</strong> Pine has been obsoleted by <a href="/Knowledge/Help/UNIX%20Commands%20and%20Concepts/Mail%20Clients/alpine">Alpine</a> and consequently is no longer available on silver. You should use alpine instead.</p>
<hr />

<p>Pine, short for &quot;Program for Internet News and Email&quot;, is designed to be easy to use and by default has a reduced command set to help with this.</p>

<p>Pine will only run on silver and is started with the following command:</p>

<pre class="console">$ pine</pre>

<p>You will be presented with the following screen:</p>

<pre class="console"><span class="reverse">  PINE 4.58   MAIN MENU                             Folder: INBOX  1 Message    </span><br /><br /><br />          ?     HELP               -  Get help using Pine<br /><br />          C     COMPOSE MESSAGE    -  Compose and send a message<br /><br />          I     MESSAGE INDEX      -  View messages in current folder<br /><br />         <span class="reverse"> L     FOLDER LIST        -  Select a folder to view         </span><br /><br />          A     ADDRESS BOOK       -  Update address book<br /><br />          S     SETUP              -  Configure Pine Options<br /><br />          Q     QUIT               -  Leave the Pine program<br /><br /><br /><br /><br />   Copyright 1989-2003.  PINE is a trademark of the University of Washington.<br />                     <span class="reverse">[Folder &quot;INBOX&quot; opened with 1 message]</span><br /><span class="reverse">?</span> Help                     <span class="reverse">P</span> PrevCmd                 <span class="reverse">R</span> RelNotes<br /><span class="reverse">O</span> OTHER CMDS <span class="reverse">&gt;</span> [ListFldrs] <span class="reverse">N</span> NextCmd                 <span class="reverse">K</span> KBLock</pre>

<p>To select a menu item, you can either press the key indicated (e.g. for help, it&#39;s ?; to compose a message, C) or move the highlight to the appropriate option with the up and down arrow keys, then press Enter.</p>

<p>If you just want to send an email, you can use:</p>

<pre class="console">$ pine <em>[email address]</em></pre>

<p>This will produce the following screen:</p>

<pre class="console"><span class="reverse">  PINE 4.58   COMPOSE MESSAGE                    Folder: (CLOSED)  No Messages  </span><br /><br />To      : test@test.com<br /><span class="reverse">Cc      :</span><br />Attchmnt:<br />Subject :<br />----- Message Text -----<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><span class="reverse">^G</span> Get Help  <span class="reverse">^X</span> Send      <span class="reverse">^R</span> Rich Hdr  <span class="reverse">^Y</span> PrvPg/Top <span class="reverse">^K</span> Cut Line  <span class="reverse">^O</span> Postpone<br /><span class="reverse">^C</span> Cancel    <span class="reverse">^D</span> Del Char  <span class="reverse">^J</span> Attach    <span class="reverse">^V</span> NxtPg/End <span class="reverse">^U</span> UnDel Line<span class="reverse">^T</span> To AddrBk<br /></pre>

<p>You may notice that this bears a striking resemblance to pico or <a href="../Text%20Editors/nano">nano</a>. This is because pine does actually use the pico text editor to compose mail (and nano is based on pico).</p>

<p>You will see that the <strong>Cc</strong> line is highlighted as this is what you are expected to enter next. If you want to enter multiple email addresses, separate them with commas. When you have finished a field, press enter. Once you get to the point that <strong>----- Message Text -----</strong> is highlighted, you can type the message itself.</p>

<p>When you have finished, press Ctrl-X to send the message, Ctrl-O to postpone it or Ctrl-C to cancel. If you postponed the message, you can go back to it by choosing the Compose message option and saying &quot;yes&quot; when asked if you want to continue the postponed mesage.</p>