<p>The following is an attempt to explain some of the more unusual and esoteric phrases and acronyms that regularly get used on milliways.&nbsp;</p>
<p><strong>AIUI</strong> (As I Understand It) - This is how the person speaking understands the topic, although it might not be correct. </p>
<p><strong>AFAIK</strong> (As Far As I Know) - This is as much as the person speaking knows, it might not be everything or up to date. </p>
<p><strong>FFS</strong> (For F*cks Sake) - An expression of annoyance or frustration.</p>
<p><strong>FTW</strong> (For The Win) - Expressing that something is particularly good or useful.</p>
<p><strong>IIRC </strong>(If I Remeber/Recall Correctly) - The person speaking is relating something from memory, but it is possible they have misremembered.</p>
<p><strong>ISTR </strong>(I Seem To Remember/Recall) - The person speaking thinks they have remembered something correctly but are not completely certain.</p>
<p><strong>KUATB</strong> (Keep Up At The Back) - The thing you just mentioned is old news and has already been debated, probably several times, before you arrived.</p>
<p><strong>MROD</strong> (Magic Roundabout Of Death) - The mechanism for ejecting someone entirely from Milliways.</p>
<p><strong>s/foo/bar/</strong> - This is the regex search and replace function, as used by the "sed" and "vi" commands, in this case "foo" would be replaced by "bar".&nbsp; It is used as a method of correcting an error in a previous line, usually but not always the previous line spoken by the person using it. It is also used for humorous effect. </p>
<p><strong>MW</strong> (Milliways) - This is how people on Milliways usually refer to it.</p>
<p><strong>(N)SFW</strong> ((Not) Safe For Work) -&nbsp;When posting URIs, anything which may be considered offensive is usually tagged as "NSFW" since most people would be repremanded for viewing such sites while at work.&nbsp; Similarly, links which sound dodgy, but are infact ok, may be tagged as "SFW" to clarify that there is nothing offensive on them.</p>
<p><strong>room x</strong> - Milliways is divided into different rooms, numbered from 0 (where almost everything happens) to 65535.</p>
<p><strong>(The) room</strong> - The SUCS Room on the ground floor of the Student Union building</p>
<p><strong>ZOD</strong> (Zebedee of Death) - A slightly less drastic ejection mechanism, simply throws them out of talker rather than out of the system entirely.</p>
<p>The usernames of almost all users are frequently abbreviated for easier and faster typing. </p>
<p>Most commonly, the first few characters of the username are used, for example "rohan" is often referred to as "ro", "Zaphod" as "Z" and "Arthur" as "art". If the name obviously contains more than one part, then often the first letter of each part is used as the abbreviation - e.g. "FireFury" becomes "ff", "rollercow" is "rc" and "willwel2000" is "ww" or "ww2k".&nbsp; </p>
<p>Exceptionally there are abbreviations that do not appear to relate to the username. For example "Thryduulf" is shortened to the logical "thr" or "thry", but also by some to "cmc" - this is because Thryduulf is an awkward sod and for historical reasons as Thryduulf's username used to be "cmckenna". </p>
<p>When a comment is directed at a particular user, their username (or an abbreviation of it) followed by "&gt;" or ":" is usually placed at the start of the line. For example you might see</p>
<pre>Arthur: dom&gt; are you in the room?<br />Psycodom: art&gt; no, but rc is. <br /></pre>
<p>Meaning that Arthur is asking whether psycodom is currently in the SUCS room. Psycodom replies that he isn't, but that rollercow is. </p>