
<p>Instead of downloading your mail to your own computer, you can read your SUCS mail in situ, by running a shell-based mail client on silver. There are a number of advantages to doing this. It saves time, as you don&#39;t have to wait for the mail to be downloaded - you can start reading it immediately (and of course, delete it if you don&#39;t want it). Also, as you are viewing it as plain text and haven&#39;t downloaded it to your computer, there is no risk whatsoever of getting viruses by reading your mail in this way.</p>

<p>First, you need to <a href="../Logging%20in%20remotely">open an SSH session to silver</a>. Once you have done this, you have a variety of mail clients to choose from. You should decide which you want to use early on and stick to it because when you save mail, it will be put into your mail client&#39;s own database or directory structure. After this has happened, it is not as easy to read the messages in another client. Note that you need to be connected to silver, as this is the machine that runs the mail server and stores the mail - you will not be able to access your mail from the shell on other SUCS machines without first sshing to silver.</p>

<p>Some of the mail clients available on silver:</p>

<ul>
<li>mutt</li>
<li><a href="../../UNIX%20Commands%20and%20Concepts/Mail%20Clients/alpine" title="Alpine">alpine</a></li>
<li>elm</li>
<li>mail</li>
</ul>