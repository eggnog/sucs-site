<div class="box">
<div class="boxhead">
<h2>Tim Clark (eclipse) - "NAT &amp; IPtables"</h2>
</div>
<div class="boxcontent">
<p>Eclipse waves his arms around... again... Oh and imparts some of his
iptables knowledge</p>
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2008-02-27/eclipse.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2008-02-27/eclipse.flv" />
<param name="image" value="/videos/talks/2008-02-27/eclipse.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2008-02-27/eclipse.png" />
</object>
</div>
<p><strong>Length: </strong>13m 42s</p>
<p><strong>Video: </strong><a href="/videos/talks/2008-02-27/eclipse.ogg" title="720x576 Ogg Theora - 58MB">720x576</a> (Ogg Theora, 58MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
